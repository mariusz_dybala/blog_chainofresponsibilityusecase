using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MvvmCross.Forms.Presenters.Attributes;
using MvvmCross.Forms.Views;
using ChainOfResponsibilityUsecase.Core.ViewModels.Home;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace ChainOfResponsibilityUsecase.UI.Pages
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    [MvxContentPagePresentation(WrapInNavigationPage = true)]
    public partial class HomePage : MvxContentPage<HomeViewModel>
    {
        public HomePage()
        {
            InitializeComponent();
        }

        protected override void OnViewModelSet()
        {
            base.OnViewModelSet();

            var binding = this.CreateBindingSet();

            binding.Bind(FirstName).For(x => x.Text).To(x => x.FirstName);
            binding.Bind(FirstNameErrorMessage).For(x => x.Text)
                .To(x => x.FirstNameErrorMessage);

            binding.Bind(LastName).For(x => x.Text).To(x => x.LastName);
            binding.Bind(LastNameErrorMessage).For(x => x.Text)
                .To(x => x.LastNameErrorMessage);

            binding.Bind(Phone).For(x => x.Text).To(x => x.Phone);
            binding.Bind(PhoneErrorMessage).For(x => x.Text)
                .To(x => x.PhoneErrorMessage);

            binding.Bind(Email).For(x => x.Text).To(x => x.Email);
            binding.Bind(EmailErrorMessage).For(x => x.Text)
                .To(x => x.EmailErrorMessage);

            binding.Apply();
        }

        protected override void OnAppearing()
        {
            base.OnAppearing();

            if (Application.Current.MainPage is NavigationPage navigationPage)
            {
                navigationPage.BarTextColor = Color.White;
                navigationPage.BarBackgroundColor = (Color)Application.Current.Resources["PrimaryColor"];
            }
        }
    }
}
