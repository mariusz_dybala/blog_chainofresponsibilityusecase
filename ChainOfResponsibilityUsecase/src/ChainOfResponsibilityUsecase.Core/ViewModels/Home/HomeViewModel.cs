using ChainOfResponsibilityUsecase.Core.ViewModels.Services;
using ChainOfResponsibilityUsecase.Core.ViewModels.Services.Interfaces;

namespace ChainOfResponsibilityUsecase.Core.ViewModels.Home
{
    public class HomeViewModel : BaseViewModel
    {
        private readonly IValidator _firstNameFieldValidator =
            new NotNullOrEmptyTextValidator(new TextLengthValidator
                (new OnlyLettersValidator()));

        private readonly IValidator _lastNameFieldValidator =
            new NotNullOrEmptyTextValidator(
                new TextLengthValidator(
                    new OnlyLettersValidator()));

        private readonly IValidator _emailFieldValidator =
            new NotNullOrEmptyTextValidator(
                new EmailValidator());

        private readonly IValidator _phoneFieldValidator = new NotNullOrEmptyTextValidator(
            new OnlyDigitsValidator());

        private string _firstName;
        private string _firstNameErrorMessage;
        private string _lastName;
        private string _lastNameErrorMessage;
        private string _phone;
        private string _phoneErrorMessage;
        private string _email;
        private string _emailErrorMessage;

        public string FirstName
        {
            get => _firstName;
            set
            {
                SetProperty(ref _firstName, value);
                OnFirstNameChanged();
            }
        }

        public string FirstNameErrorMessage
        {
            get => _firstNameErrorMessage;
            set => SetProperty(ref _firstNameErrorMessage, value);
        }

        public string LastName
        {
            get => _lastName;
            set
            {
                SetProperty(ref _lastName, value);
                OnLastNameChanged();
            }
        }

        public string LastNameErrorMessage
        {
            get => _lastNameErrorMessage;
            set => SetProperty(ref _lastNameErrorMessage, value);
        }

        public string Phone
        {
            get => _phone;
            set
            {
                SetProperty(ref _phone, value);
                OnPhoneChanged();
            }
        }

        public string PhoneErrorMessage
        {
            get => _phoneErrorMessage;
            set => SetProperty(ref _phoneErrorMessage, value);
        }

        public string Email
        {
            get => _email;
            set
            {
                SetProperty(ref _email, value);
                OnEmailChanged();
            }
        }

        public string EmailErrorMessage
        {
            get => _emailErrorMessage;
            set => SetProperty(ref _emailErrorMessage, value);
        }

        private void OnFirstNameChanged()
        {
            FirstNameErrorMessage = _firstNameFieldValidator.Validate(FirstName);
        }

        private void OnLastNameChanged()
        {
            LastNameErrorMessage = _lastNameFieldValidator.Validate(LastName);
        }

        private void OnPhoneChanged()
        {
            PhoneErrorMessage = _phoneFieldValidator.Validate(Phone);
        }

        private void OnEmailChanged()
        {
            EmailErrorMessage = _emailFieldValidator.Validate(Email);
        }
    }
}
