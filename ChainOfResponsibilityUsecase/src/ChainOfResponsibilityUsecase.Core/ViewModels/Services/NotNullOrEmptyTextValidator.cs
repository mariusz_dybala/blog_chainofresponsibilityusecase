using ChainOfResponsibilityUsecase.Core.ViewModels.Services.Interfaces;

namespace ChainOfResponsibilityUsecase.Core.ViewModels.Services
{
    public class NotNullOrEmptyTextValidator : IValidator
    {
        private readonly IValidator _nextSuccessor;

        public NotNullOrEmptyTextValidator(IValidator nextSuccessor = null)
        {
            _nextSuccessor = nextSuccessor;
        }

        public string Validate(string inputString)
        {
            if (string.IsNullOrEmpty(inputString))
            {
                return "Field cannot be empty";
            }

            if (_nextSuccessor != null)
            {
                return _nextSuccessor.Validate(inputString);
            }
            return string.Empty;
        }
    }
}
