using System;
using System.Linq;
using ChainOfResponsibilityUsecase.Core.ViewModels.Services.Interfaces;

namespace ChainOfResponsibilityUsecase.Core.ViewModels.Services
{
    public class OnlyLettersValidator : IValidator
    {
        private readonly IValidator _nextSuccessor;

        public OnlyLettersValidator(IValidator nextSuccessor = null)
        {
            _nextSuccessor = nextSuccessor;
        }

        public string Validate(string inputString)
        {
            if (!inputString.All(char.IsLetter))
            {
                return "This filed should contain only letters";
            }

            if (_nextSuccessor != null)
            {
                return _nextSuccessor.Validate(inputString);
            }

            return string.Empty;
        }
    }
}
