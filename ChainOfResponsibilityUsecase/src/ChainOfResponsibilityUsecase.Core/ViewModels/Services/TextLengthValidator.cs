using ChainOfResponsibilityUsecase.Core.ViewModels.Services.Interfaces;

namespace ChainOfResponsibilityUsecase.Core.ViewModels.Services
{
    public class TextLengthValidator : IValidator
    {
        private readonly IValidator _nextSuccessor;

        public TextLengthValidator(IValidator nextSuccessor = null)
        {
            _nextSuccessor = nextSuccessor;
        }

        public string Validate(string inputString)
        {
            if (inputString.Length <= 2 || inputString.Length >= 10)
            {
                return "Text length should be greater than 2 and less than 10";
            }

            if (_nextSuccessor != null)
            {
                return _nextSuccessor.Validate(inputString);
            }
            return string.Empty;
        }
    }
}
