namespace ChainOfResponsibilityUsecase.Core.ViewModels.Services.Interfaces
{
    public interface IValidator
    {
        string Validate(string inputString);
    }
}
