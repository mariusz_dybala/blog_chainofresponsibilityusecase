using System.Text.RegularExpressions;
using ChainOfResponsibilityUsecase.Core.ViewModels.Services.Interfaces;

namespace ChainOfResponsibilityUsecase.Core.ViewModels.Services
{
    public class EmailValidator : IValidator
    {
        private readonly IValidator _nextSuccessor;

        public EmailValidator(IValidator nextSuccessor = null)
        {
            _nextSuccessor = nextSuccessor;
        }

        public string Validate(string inputString)
        {
            var regex = new Regex(@"^([\w\.\-]+)@([\w\-]+)((\.(\w){2,3})+)$");

            if (!regex.IsMatch(inputString))
            {
                return "This is not a valid email address";
            }

            if (_nextSuccessor != null)
            {
                return _nextSuccessor.Validate(inputString);
            }
            return string.Empty;
        }
    }
}
