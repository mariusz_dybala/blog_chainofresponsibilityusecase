using System.Linq;
using ChainOfResponsibilityUsecase.Core.ViewModels.Services.Interfaces;

namespace ChainOfResponsibilityUsecase.Core.ViewModels.Services
{
    public class OnlyDigitsValidator : IValidator
    {
        private readonly IValidator _nextSuccessor;

        public OnlyDigitsValidator(IValidator nextSuccessor = null)
        {
            _nextSuccessor = nextSuccessor;
        }

        public string Validate(string inputString)
        {
            if (!inputString.All(char.IsDigit))
            {
                return "Phone number should contain only digits";
            }

            if (_nextSuccessor != null)
            {
                return _nextSuccessor.Validate(inputString);
            }
            return string.Empty;
        }
    }
}
