﻿using MvvmCross.IoC;
using MvvmCross.ViewModels;
using ChainOfResponsibilityUsecase.Core.ViewModels.Home;

namespace ChainOfResponsibilityUsecase.Core
{
    public class App : MvxApplication
    {
        public override void Initialize()
        {
            CreatableTypes()
                .EndingWith("Service")
                .AsInterfaces()
                .RegisterAsLazySingleton();

            RegisterAppStart<HomeViewModel>();
        }
    }
}
